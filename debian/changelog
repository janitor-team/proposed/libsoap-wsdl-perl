libsoap-wsdl-perl (3.004-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 15:30:18 +0100

libsoap-wsdl-perl (3.004-1) unstable; urgency=medium

  * Import upstream version 3.004.
    Fixes "FTBFS: test failures with libtimedate-perl 2.3100-1"
    (Closes: #949242)
  * Drop patches which were applied upstream: makefile.pl-exe_files.patch,
    pod-spelling.patch, pod-whatis.patch.
  * Drop remaining patch use-Test::XML.patch which shouldn't be needed
    anymore according to CPAN RT#74257.
  * Update debian/upstream/metadata.
  * Update years of packaging copyright.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 4.4.1.
  * Drop unneeded alternative build dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Mon, 20 Jan 2020 19:32:50 +0100

libsoap-wsdl-perl (3.003-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Fix t/SOAP/WSDL/Server/Mod_Perl2.t failures in autopkgtest environment
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.4
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sat, 05 May 2018 19:35:40 +0300

libsoap-wsdl-perl (3.003-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Use (file-)rename instead of (p)rename. (Closes: #825636)
  * Update years of packaging copyright.
  * Add a patch to install the wsdl2perl script.
  * Update remaining patches. Add more spelling fixes, forward upstream.
  * Bump debhelper compatibility level to 9.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 May 2016 18:49:02 +0200

libsoap-wsdl-perl (3.003-1) unstable; urgency=medium

  * Team upload

  * Add debian/upstream/metadata
  * Imported Upstream version 3.003
  * add upstream Git repository URL to debian/usptream/metadata
  * Add another upstream copyright holder
  * Drop lvalue-modification-5.18.patch, more correct fix released upstream
  * Drop load-with-Class::Load.patch, applied upstream
  * Drop prefix_from_namespace.patch, issue fixed upstream
  * Drop fix-HeaderFault-base.patch, applied upstream
  * Refress pod-spelling.patch
  * Add build dependency on libcgi-pm-perl | perl (<< 5.19)

 -- Damyan Ivanov <dmn@debian.org>  Fri, 12 Jun 2015 12:37:44 +0000

libsoap-wsdl-perl (2.00.10-3) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtestable
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * add two ocurrences of "allow to" -> "allow one to" to pod-spelling.patch
  * suggest libapache2-mod-perl2 which is needed for the ModPerl2 WSDL server
    module
  * skip SOAP/WSDL/Server/Mod_Perl2.pm from autopkg-test/syntax.t as it
    requires mod_perl, which is only suggested
  * add patch fixing the base class of SOAP::WSDL::SOAP::HeaderFault

 -- Damyan Ivanov <dmn@debian.org>  Mon, 08 Jun 2015 14:42:44 +0000

libsoap-wsdl-perl (2.00.10-2) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Damyan Ivanov ]
  * add libtest-xml-perl to build-dependencies enabling additional tests
  * add patches fixing invalid lvalue assignments and hash randomization
    issues with perl 5.18. Closes: #720964
  * update pod-spelling.patch with two occurences of "allows to"
  * declare conformance with Policy 3.9.5
  * remove trailing shash from metacpan URLs

 -- Damyan Ivanov <dmn@debian.org>  Wed, 15 Jan 2014 12:39:08 +0200

libsoap-wsdl-perl (2.00.10-1) unstable; urgency=low

  * Initial release (closes: #607712).

 -- gregor herrmann <gregoa@debian.org>  Tue, 21 Dec 2010 20:03:05 +0100
